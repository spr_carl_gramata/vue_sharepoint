'use strict';

const gulp = require('gulp');
const build = require('@microsoft/sp-build-web');
build.addSuppression(`Warning - [sass] The local CSS class 'ms-Grid' is not camelCase and will not be type-safe.`);

build.configureWebpack.mergeConfig({

    additionalConfiguration: (generatedConfiguration) => {

        const VueLoaderPlugin = require('vue-loader/lib/plugin');
        const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

        const vuePlugin = new VueLoaderPlugin();
        const forkTsPlugin = new ForkTsCheckerWebpackPlugin({
            vue: true,
            tslint: true,
            formatter: 'codeframe',
            checkSyntacticErrors: false
          });

        const loadersConfigs = [{
            test: /\.vue$/, // vue
            use: [{
                loader: 'vue-loader'
            }]
        }, {
            resourceQuery: /vue&type=script&lang=ts/, // typescript
            loader: 'ts-loader',
            options: {
                appendTsSuffixTo: [/\.vue$/],
                transpileOnly: true
            }
        }, {
            resourceQuery: /vue&type=style.*&lang=scss/, // scss
            use: [
                {
                    loader: require.resolve('@microsoft/loader-load-themed-styles'),
                    options: {
                        async: true
                    }
                },
                {
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        localIdentName: '[local]_[sha1:hash:hex:8]'
                    }
                },
                'sass-loader']
        }, {
            resourceQuery: /vue&type=style.*&lang=sass/, // sass
            use: [
                {
                    loader: require.resolve('@microsoft/loader-load-themed-styles'),
                    options: {
                        async: true
                    }
                },
                {
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        localIdentName: '[local]_[sha1:hash:hex:8]'
                    }
                },
            'sass-loader?indentedSyntax']  
        }];

        generatedConfiguration.plugins.push(vuePlugin, forkTsPlugin);
        generatedConfiguration.module.rules.push(...loadersConfigs);

        return generatedConfiguration;

    }
});

let copyVueFiles = build.subTask('copy-vue-files', function (gulp, buildOptions, done) {
    gulp.src(['src/**/*.vue'])
        .pipe(gulp.dest(buildOptions.libFolder));
    done();
});

let customWatchRegistered = false; 

let watchVueFiles = build.subTask('watch-vue-files', function (gulp, buildOptions, done) {
    if (!customWatchRegistered) {

        gulp.watch('./src/**/*.vue', event => {
            gulp.src('./src/index.ts')
                .pipe(gulp.dest('./src/'));
        });

        customWatchRegistered = true;
    }
    done();
});

build.rig.addPostTypescriptTask(copyVueFiles);
build.rig.addPreBuildTask(watchVueFiles);

build.initialize(gulp);
